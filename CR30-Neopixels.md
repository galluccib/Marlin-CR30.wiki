This modification add neopixel support to the stock CR-30 4.2.10 board. This will work with both Marlin and Klipper.

You will need:
1. A strip of neopixels, I used WS2812B 144/M pixel strip.
2. A 5V buck converter capable of at least 3amps.
3. A JST-XHP 3-pin connector and one male pin.
4. A few ring terminal crip connectors.
5. 2-3' of 18AWG or 20AWG wire.
6. The printed mount for the neopixel strip [CR-30 Neopixel mount](https://www.thingiverse.com/thing:4943797)
7. EITHER the ability to solder to the neopixels OR buy a neopixel strip with the connector already attached.

## Summary of how this mod works
* The pin needed (PA7) to control RGB neopixel strips is already in use by the Y optical endstop. This pin needs to be moved to the unused pin (PA15) in the unused AUX connector.
* The connector cannot just be moved from `Optical` to `Aux`, as **`Optical` is 5V and `Aux` is 3.3V**.
* Add a new 3-pin connector for Aux, with just the 1 pin removed from the Optical plug inserted into this connector.
* Wire up the signal pin for the Neopixel strip to the pin in the Optical connector we removed.
* Wire up the buck converter to power the neopixels separately from the mainboard.
* Adjust the firmware to enable neopixel control and move the pin for the optical endstop.

## Hardware modification
![](https://github.com/adelyser/pics/blob/master/CR-30/Creality%204.2.10%20pins.jpg)
Steps:
1. Unplug the `Optical` plug from the mainboard and remove the pin closest to the edge of the board.
2. Insert the pin removed in step 1 into the new connector, in the same pin location (closest to the edge of the board).
3. Crimp a new pin on the end of a new wire (24 AWG is fine) and insert it into the `Optical` plug.
4. Plug the `Optical` plug back into it's original location.
5. Plug the NEW plug into the `Aux` location.
6. Wire up the buck converter using ring terminals to the powersupply.
7. Route the 3 new wires out of the electronics compartment and close it up.

## Marlin Firmware modification
### Grab the latest firmware from [CR-30 Users firmware](https://github.com/CR30-Users/Marlin-CR30/releases)
**In `Configuration.h`:**
* Uncomment `NEOPIXEL_LED` and change the `NEOPIXEL_PIXELS` to match the number of LEDS on your strip.
* You may also need to change the NEOPIXEL_TYPE depending on the neopixel strip you have.
```
// Support for Adafruit NeoPixel LED driver
#define NEOPIXEL_LED
#if ENABLED(NEOPIXEL_LED)
  #define NEOPIXEL_TYPE   NEO_GRB // NEO_GRBW / NEO_GRB - four/three channel driver type (defined in Adafruit_NeoPixel.h)
  #define NEOPIXEL_PIN     PA7    // LED driving pin
  //#define NEOPIXEL2_TYPE NEOPIXEL_TYPE
  //#define NEOPIXEL2_PIN    5
  #define NEOPIXEL_PIXELS 45       // Number of LEDs in the strip. (Longest strip when NEOPIXEL2_SEPARATE is disabled.)
  #define NEOPIXEL_IS_SEQUENTIAL   // Sequential display for temperature change - LED by LED. Disable to change all LEDs at once.
  #define NEOPIXEL_BRIGHTNESS 225  // Initial brightness (0-255)
  #define NEOPIXEL_STARTUP_TEST  // Cycle through colors at startup

  // Support for second Adafruit NeoPixel LED driver controlled with M150 S1 ...
  //#define NEOPIXEL2_SEPARATE
  #if ENABLED(NEOPIXEL2_SEPARATE)
    #define NEOPIXEL2_PIXELS      15  // Number of LEDs in the second strip
    #define NEOPIXEL2_BRIGHTNESS 127  // Initial brightness (0-255)
    #define NEOPIXEL2_STARTUP_TEST    // Cycle through colors at startup
  #else
    //#define NEOPIXEL2_INSERIES      // Default behavior is NeoPixel 2 in parallel
  #endif

  // Use some of the NeoPixel LEDs for static (background) lighting
  //#define NEOPIXEL_BKGD_INDEX_FIRST  0              // Index of the first background LED
  //#define NEOPIXEL_BKGD_INDEX_LAST   5              // Index of the last background LED
  //#define NEOPIXEL_BKGD_COLOR { 255, 255, 255, 0 }  // R, G, B, W
  //#define NEOPIXEL_BKGD_ALWAYS_ON                   // Keep the backlight on when other NeoPixels are off
#endif
```
**In `Configuration_adv.h`:**
* Uncomment `LED_CONTROL_MENU` and change the `LED_COLOR_PRESET` values per below:
```
  /**
   * LED Control Menu
   * Add LED Control to the LCD menu
   */
  #define LED_CONTROL_MENU
  #if ENABLED(LED_CONTROL_MENU)
    #define LED_COLOR_PRESETS                 // Enable the Preset Color menu option
    //#define NEO2_COLOR_PRESETS              // Enable a second NeoPixel Preset Color menu option
    #if ENABLED(LED_COLOR_PRESETS)
      #define LED_USER_PRESET_RED        255  // User defined RED value
      #define LED_USER_PRESET_GREEN      255  // User defined GREEN value
      #define LED_USER_PRESET_BLUE       255  // User defined BLUE value
      #define LED_USER_PRESET_WHITE      255  // User defined WHITE value
      #define LED_USER_PRESET_BRIGHTNESS 255  // User defined intensity
      #define LED_USER_PRESET_STARTUP       // Have the printer display the user preset color on startup
    #endif
    #if ENABLED(NEO2_COLOR_PRESETS)
      #define NEO2_USER_PRESET_RED        255  // User defined RED value
      #define NEO2_USER_PRESET_GREEN      128  // User defined GREEN value
      #define NEO2_USER_PRESET_BLUE         0  // User defined BLUE value
      #define NEO2_USER_PRESET_WHITE      255  // User defined WHITE value
      #define NEO2_USER_PRESET_BRIGHTNESS 255  // User defined intensity
      //#define NEO2_USER_PRESET_STARTUP       // Have the printer display the user preset color on startup for the second strip
    #endif
  #endif
```

**In `src/pins/stm32f1/pins_CREALITY_V4210.h`:**
* Change the `Y_STOP_PIN`:
```
#define Y_STOP_PIN                          PA15
```


## Klipper Configuration
**In printer.cfg:***
* Change the `stepper_y` endstop pin
* Enable neopixel support
```
[stepper_y]
endstop_pin: ^!PA15

# Neopixel LED support
[neopixel led_neopixel]
pin: PA7
chain_count: 45
```