Welcome to the Marlin-CR30 wiki!

Here you can find a variety of information, guides and mods for the Creality CR-30 3DPrintMill printer.

* [4.2.10 mainboard pin map](https://github.com/adelyser/Marlin-CR30/wiki/Creality-4.2.10-pins)
* [Silence them fans!](https://github.com/adelyser/Marlin-CR30/wiki/Silent-fans-on-CR-30-while-idle)
* [Enable the TMC uarts!](https://github.com/adelyser/Marlin-CR30/wiki/Creality-4.2.10-UART-mod-(CR-30))
* [Neopixels!](https://github.com/adelyser/Marlin-CR30/wiki/CR-30-Neopixels)