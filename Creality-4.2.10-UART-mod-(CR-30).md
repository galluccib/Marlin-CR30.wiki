This modification enables UART communication with the on-board TMC2208 stepper drivers. Enabling the UART comms allows:
* Stepper currents to be changed with the M906 command.
* Linear Advance by disabling StealthChop with the M569 command.

![](https://github.com/adelyser/pics/blob/master/CR-30/Creality-4.2.10-wired.jpg)
## Hardware modification
![](https://github.com/adelyser/pics/blob/master/CR-30/Creality-4.2.10-diagram.jpg)
Steps:
1. Remove 4 resistors: R17, R18, R19, R20
2. Solder a wire to the pad from the above resistors, closest pad to the driver.
3. Connect the other ends of the wires to pins on the board according to the pictures. I used the 2 center pins of P3 and the IN and OUT pins of P1 (labled on the bottom of the board).

## Firmware modification
### Grab the latest firmware from [CR-30 Users firmware](https://github.com/CR30-Users/Marlin-CR30/releases)
**In `Configuration.h`:**
* Change TMC2208_STANDALONE to TMC2208 for the X, Y, Z and E0 steppers:
```
#define X_DRIVER_TYPE  TMC2208
#define Y_DRIVER_TYPE  TMC2208
#define Z_DRIVER_TYPE  TMC2208
#define E0_DRIVER_TYPE TMC2208
```

**In `Configuration_adv.h`:**
* Uncomment `//#define LIN_ADVANCE`:
```
#define LIN_ADVANCE
```

### NOTE: The following is already done in kageurufu's firmware linked above and is only for reference.
**In `src/pins/stm32f1/pins_CREALITY_V4210.h`:**
* Disable the BL_TOUCH pins:
```
//#define SERVO0_PIN                          PB0   // BLTouch OUT
//#define Z_MIN_PROBE_PIN                     PB1   // BLTouch IN
```
* Add the pin definitions for the serial connections as follows:

```
#if HAS_TMC_UART
  #define X_SERIAL_TX_PIN                   PB0
  #define X_SERIAL_RX_PIN                   PB0

  #define Y_SERIAL_TX_PIN                   PB1
  #define Y_SERIAL_RX_PIN                   PB1

  #define Z_SERIAL_TX_PIN                   PA13
  #define Z_SERIAL_RX_PIN                   PA13

  #define E0_SERIAL_TX_PIN                  PA14
  #define E0_SERIAL_RX_PIN                  PA14

  #define TMC_BAUD_RATE                    19200
#endif
```

## Printer setup
* After flashing the printer with the compiled firmware, go to `Configuration -> Advanced Settings -> TMC Drivers -> Stepping Mode` and disable Stealthchop for the E driver.
* Then go to `Configuration -> Advanced Settings' and select `Save Settings` so the TMC driver settings persist.