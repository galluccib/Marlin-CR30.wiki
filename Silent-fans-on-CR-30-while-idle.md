When we aren't printing with our printers, we typically would rather our printer be silent than sounding like an IT server room. By using the latest firmware and moving both fan connectors, you can enjoy the peaceful silence! However, shouldn't you really be keeping that new printer active? :)

* The hotend fan will automatically come on when the hotend temperature reaches 50C, and will automatically shutoff when the hotend temperature drops below 50C.
* The controller fan (mainboard) will automatically come on when you move a stepper motor, and will shutoff 5 minutes after the motors have stopped moving.

First, you need to flash the latest firmware found here:
https://github.com/CR30-Users/Marlin-CR30/releases

This firmware works with the fans in both the factory location and also with the new controlled locations, so don't worry if you haven't moved the connectors!

Now, simply unplug the fan connectors from the mainboard and move them as follows:

1. Move the plug from FAN1 to K-FAN2
1. Move the plug from FAN2 to K-FAN3

![](https://github.com/adelyser/pics/blob/master/CR-30/CR-30%20Controlled%20Fan%20mod.jpg)
